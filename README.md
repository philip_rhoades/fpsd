# A Fedora Discourse Podman Container (with systemd) 

## Install required system and development packages using a Docker file

cd fedora_systemd_discourse/src

dnf install policycoreutils

podman build -t f33_systemd_discourse .

setsebool -P container_manage_cgroup true

## Run podman

### For host postgresql

podman run -it --net=host --name f33_systemd_discourse f33_systemd_discourse /sbin/init

### For podman postgresql

podman run -it -v /var/run/postgresql:/var/run/postgresql --net=host --name f33_systemd_discourse f33_systemd_discourse /sbin/init

## From another terminal

podman exec -it f33_systemd_discourse bash

## Install required npm packages

[container]# npm install -g svgo yarn

## Install and setup postgres

### For local container-based postgres

[container]# dnf install postgresql-server

[container]# postgresql-setup --initdb --unit postgresql

[container]# systemctl enable postgresql

[container]# systemctl start postgresql

[container]# su -l postgres -c "createuser -s -d discourse"

### For host-based postgres

[host]# dnf install postgresql-server

[container]# systemctl disable postgresql

[host]# systemctl enable postgresql

[host]# postgresql-setup --initdb --unit postgresql

[host]# systemctl enable postgresql

[host]# systemctl start postgresql

[host]# su -l postgres -c "createuser -s -d discourse"

[host]# adduser discourse

[container]# adduser -u \<UID\> -U discourse

- where UID is the ID of the discourse user on the host

## Install and setup redis

[container]# systemctl enable redis

[container]# systemctl start redis

## Installing rbenv, ruby-build, and ruby

[container]# git clone https://github.com/rbenv/rbenv.git ~/.rbenv

[container]# cd ~/.rbenv && src/configure && make -C src

[container]# ~/.rbenv/bin/rbenv init

[container]# printf 'export PATH="$HOME/.rbenv/bin:$PATH"\n' >> ~/.bashrc

[container]# printf 'eval "$(rbenv init - --no-rehash)"\n' >> ~/.bashrc

[container]# source ~/.bashrc

[container]# git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

## Confirm the install is correct

[container]# curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash

[container]# mkdir /tmp1

[container]# export TMPDIR=/tmp1

[container]# rbenv install 2.7.1

[container]# rbenv global 2.7.1

[container]# rbenv rehash

## Install Ruby dependencies

[container]# gem update --system

[container]# gem install bundler mailcatcher rails
- If necessary change UID to be the same as the UID for the discourse user on the host

## Clone Discourse code

[container]# su - discourse

[container]# git clone https://github.com/discourse/discourse.git

[container]# cd ~/discourse/

## Install Discourse dependencies

[container]# bundle install --path vendor/bundle

## Create the required databases and load the schema

[container]# bundle exec rake db:create db:migrate

[container]# RAILS_ENV=test bundle exec rake db:create db:migrate

## Test installation by running the tests

This takes a long time and I saw lots of warnings . .

[container]# bundle exec rake autospec

## Run the application

[container]# bundle exec rails server -p 3001

## You should now be able to see the Discourse setup page at http://localhost:3001

## Create admin user

[container]# bundle exec rake admin:create

- You will be asked for Email, Password and Confirm Password.
- After providing required information a new account will be created with random username (based on email address).
- Now you will be asked: Do you want to grant Admin privileges to this account? (Y/n). Press enter to continue.
- If it worked, you'll see Your account now has Admin privileges!.
- This command can also be used for resetting the admin password

## You should now be able to login as admin to the Discourse site at http://localhost:3001/

